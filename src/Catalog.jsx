import React, {Component} from 'react';


export default class Catalog extends Component {

	constructor() {
		super() 
			this.state = {
				length: 0,
				products: []
			}
		this.handleAdd = this.handleAdd.bind(this)
	}
	handleAdd() {
		const newProducts = this.state.products.concat([
			{
				id: this.state.length + 1,
				title: prompt('Masukkan nama produk'),
				price: prompt('Masukkan harga produk')
			}
		])
		this.setState((state) => ({
			length: state.length + 1,
			products: newProducts
		}))
	}

	handleRemove(id) {
		let newProducts = this.state.products.filter(
			(product) => product.id !== id
		)
		this.setState({
			products: newProducts
		})
	}

	componentDidMount() {
		fetch('https://fakestoreapi.com/products?limit=5')
		.then(res => res.json())
		.then(res => this.setState({
			length: res.length,
			products: res
		}))
	}
	render () {
		const { products } = this.state
		const productItem = products.map((product) =>
			<div className="container">
				<div key={product.id} class="card" style={{ width: "350px", cursor: "pointer", float: "left", }}>
					<img src={product.image} style={{ width: "250px" }} className="card-img-top" alt=""/>
						<div class="card-body">
							<h5 class="card-title">{product.title}</h5>
							<p class="card-text">Rp{product.price}</p>
							<button className="btn btn-danger" onClick={() => this.handleRemove(product.id)}>Delete Produk</button>
						<button className="btn btn-primary" onClick={this.handleAdd}>Tambah produk</button>
						</div>
			</div>
			</div>
			
					)
		return(
			<div>
				{productItem}
			</div>
		)
	}
}


	
	